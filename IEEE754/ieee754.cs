﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEEE754
{
    class ieee754
    {
        byte[] hexSingle;
        byte[] hexDouble;
        float decSingle;
        double decDouble;

        #region properties
        public ieee754()
        {
        }

        public byte[] HexSingle
        {
            get
            {
                return hexSingle;
            }

            set
            {
                hexSingle = value;
                singleToDoubleHex();
                HexToDec();
            }
        }
        
        public byte[] HexDouble
        {
            get
            {
                return hexDouble;
            }

            set
            {
                hexDouble = value;
                HexToDec();
            }
        }

        public double DecDouble
        {
            get
            {
                return this.decDouble;
            }

            set
            {
                this.decDouble = value;
                this.decSingle = (float)this.decDouble;
                DecToHex();
            }
        }

        public float DecSingle
        {
            get
            {
                return this.decSingle;
            }

            set
            {
                this.decSingle = value;
                this.decDouble = this.decSingle;
                DecToHex();
            }
        }
        #endregion


        private void singleToDoubleHex()
        {
            this.hexDouble = BitConverter.GetBytes((double)BitConverter.ToSingle(this.hexSingle, 0));
        }

        private void DecToHex()
        {
            DecToDouble();
            DecToSingle();
        }

        private void HexToDec()
        {
            SingleToDec();
            DoubleToDec();
        }

        private void DecToDouble()
        {
            this.hexDouble = BitConverter.GetBytes(decDouble);
            Array.Reverse(this.hexDouble);
        }

        private void DecToSingle()
        {
            this.hexSingle = BitConverter.GetBytes(decSingle);
            Array.Reverse(this.hexSingle);
        }

        private void SingleToDec()
        {
            this.decSingle = BitConverter.ToSingle(this.hexSingle, 0);
        }

        private void DoubleToDec()
        {
            this.decDouble = BitConverter.ToDouble(this.hexDouble, 0);
        }


        public static ieee754 operator +(ieee754 a, ieee754 b)
        {
            ieee754 rez = new ieee754();
            rez.DecDouble = a.DecDouble + b.decDouble;
            return rez;
        }

        public static ieee754 operator -(ieee754 a, ieee754 b)
        {
            ieee754 rez = new ieee754();
            rez.DecDouble = a.DecDouble - b.DecDouble;
            return rez;
        }

        public static ieee754 operator *(ieee754 a, ieee754 b)
        {
            ieee754 rez = new ieee754();
            rez.DecDouble = a.DecDouble * b.DecDouble;
            return rez;
        }

        public static ieee754 operator /(ieee754 a, ieee754 b)
        {
            ieee754 rez = new ieee754();
            rez.DecDouble = a.DecDouble / b.DecDouble;
            return rez;
        }
    }
}
 