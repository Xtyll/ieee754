﻿using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;

namespace IEEE754
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ieee754 fstArg = new ieee754();
        ieee754 sndArg = new ieee754();
        ieee754 rez = new ieee754();
        Regex rgx = new Regex("-");


        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnToHex_click(object sender, RoutedEventArgs e)
        {
            ToHex();
        }

        private void btnToDec_Click(object sender, RoutedEventArgs e)
        {
            ToDec();
        }

        private byte[] StrToByteArraySingle(string text)
        {
            byte[] a = new byte[4];
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = Convert.ToByte(text.Substring(i * 2, 2), 16);
            }
            Array.Reverse(a);
            return a;
        }

        private byte[] StrToByteArrayDouble(string text)
        {
            byte[] a = new byte[8];
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = Convert.ToByte(text.Substring(i * 2, 2), 16);
            }
            Array.Reverse(a);
            return a;
        }

        private void tbxDec_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tbx = (TextBox)sender;
            string text = tbx.Text;
            int flag = 0;
            Regex rgx1 = new Regex(@"[\.\,]");
            text = rgx1.Replace(text, ".");
            int cursorPos = tbx.CaretIndex;
            if (text.Length > 0 && cursorPos != 0)
            {
                Char last = text[cursorPos - 1];
                if (last == '.')
                {
                    var a = rgx1.Matches(text).Count;
                    if (a > 1)
                    {
                        text = text.Remove(cursorPos - 1, 1);
                        flag = -1;
                    }
                }
                else if (!Char.IsNumber(last))
                {
                    text = text.Remove(cursorPos - 1, 1);
                    flag = -1;
                }
            }

            tbx.TextChanged -= tbxDec_TextChanged;
            tbx.Text = text;
            tbx.CaretIndex = cursorPos + flag;
            tbx.TextChanged += tbxDec_TextChanged;
        }

        private void tbxHex_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tbx = (TextBox)sender;
            string text = tbx.Text;
            int cursorPos = tbx.CaretIndex;
            text = text.ToUpper();
            Regex rgx = new Regex(@"[^A-F\d]");
            text = rgx.Replace(text, "");
            tbx.TextChanged -= tbxHex_TextChanged;
            tbx.Text = text;
            tbx.CaretIndex = cursorPos;
            tbx.TextChanged += tbxHex_TextChanged;
        }

        private void rdbDouble_Checked(object sender, RoutedEventArgs e)
        {
            clearTextBoxes();
            tbxHexFst.MaxLength = 16;
            tbxHexSnd.MaxLength = 16;
        }

        private void rdbSingle_Checked(object sender, RoutedEventArgs e)
        {
            clearTextBoxes();
            tbxHexFst.MaxLength = 8;
            tbxHexSnd.MaxLength = 8;
        }

        private void clearTextBoxes()
        {
            tbxDecFst.Text = "";
            tbxDecSnd.Text = "";
            tbxDecRez.Text = "";
            tbxHexFst.Text = "";
            tbxHexSnd.Text = "";
            tbxHexRez.Text = "";
        }

        private void ToHex()
        {
            try
            {
                fstArg.DecDouble = Convert.ToDouble(tbxDecFst.Text);
                if (rdbSingle.IsChecked == true)
                {
                    tbxHexFst.Text = rgx.Replace(BitConverter.ToString(fstArg.HexSingle), "");
                }
                else
                {
                    tbxHexFst.Text = rgx.Replace(BitConverter.ToString(fstArg.HexDouble), "");
                }
            }
            catch (Exception) { }
            try
            {
                sndArg.DecDouble = Convert.ToDouble(tbxDecSnd.Text);
                if (rdbSingle.IsChecked == true)
                {
                    tbxHexSnd.Text = rgx.Replace(BitConverter.ToString(sndArg.HexSingle), "");
                }
                else
                {
                    tbxHexSnd.Text = rgx.Replace(BitConverter.ToString(sndArg.HexDouble), "");
                }
            }
            catch (Exception) { }
        }

        private void ToDec()
        {
            try
            {
                if (rdbSingle.IsChecked == true)
                {
                    fstArg.HexSingle = StrToByteArraySingle(tbxHexFst.Text);
                    tbxDecFst.Text = fstArg.DecSingle.ToString();
                }
                else
                {
                    fstArg.HexDouble = StrToByteArrayDouble(tbxHexFst.Text);
                    tbxDecFst.Text = fstArg.DecDouble.ToString();
                }
            }
            catch (Exception) { }
            try
            {
                if (rdbSingle.IsChecked == true)
                {
                    sndArg.HexSingle = StrToByteArraySingle(tbxHexSnd.Text);
                    tbxDecSnd.Text = sndArg.DecSingle.ToString();
                }
                else
                {
                    sndArg.HexDouble = StrToByteArrayDouble(tbxHexSnd.Text);
                    tbxDecSnd.Text = sndArg.DecDouble.ToString();
                }
            }
            catch (Exception) { }
        }

        private void btnActin_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            
            if (tbxDecFst.Text!= "" && tbxDecSnd.Text != "")
            {
                ToHex();
                switch (btn.Content.ToString())
                {
                    case "+":
                        rez = fstArg + sndArg;
                        break;
                    case "-":
                        rez = fstArg - sndArg;
                        break;
                    case "*":
                        rez = fstArg * sndArg;
                        break;
                    case "/":
                        rez = fstArg / sndArg;
                        break;
                    default: break;
                }
                if (rdbSingle.IsChecked == true)
                {
                    tbxDecRez.Text = rez.DecSingle.ToString();
                    tbxHexRez.Text = rgx.Replace(BitConverter.ToString(rez.HexSingle), "");
                }
                else
                {
                    tbxDecRez.Text = rez.DecDouble.ToString();
                    tbxHexRez.Text = rgx.Replace(BitConverter.ToString(rez.HexDouble), "");
                }
            }            
        }

        private void btnRunScripting_Click(object sender, RoutedEventArgs e)
        {
            scripting scr = new scripting();
            scr.Show();
        }
    }
}
